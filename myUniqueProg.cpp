#include <iostream>
#include <cstdlib>

using namespace std;

static unsigned long long int fibos[100], fiboList[100];
void intro();

class List{
	private:
		typedef struct node{
			int data;
			node* next;
		}* nodePtr;
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
	
	public:
		List();
		void AddNode(int addData);
		void DeleteNode(int delData);
		void PrintList();
		void EditNode(int data, int newData);
		int FindData(int Data);
		
};

List::List(){
	head = NULL;
	curr = NULL;
	temp = NULL;
}

void List::AddNode(int addData){
	nodePtr n = new node;
	n->next = NULL;
	n->data = addData;
	if(head != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
	}
	else{
		head = n;
	}
}

void List::DeleteNode(int delData){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	while (curr != NULL && curr->data != delData){
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL){
		cout<<delData<<" was not in the List\n";
		intro();
	}
	else if (curr==head){
		head=head->next;
	}
	else{
		delPtr = curr;
		curr = curr->next;
		temp->next = curr;
	}
	
	delete delPtr;
}

void List::PrintList(){
	curr = head;
	while (curr != NULL){
		cout<<curr->data<<endl;
		curr = curr->next;
	}
}
void List::EditNode(int data, int newData){
	curr = head;
	while (curr->data != data){
		curr = curr->next;
	}
	if (curr){
		curr->data = newData;
	}
}

int List::FindData(int Data){
	curr = head;
	int currIndex = 1;
	while (curr && curr->data != Data){
		curr = curr->next;
		currIndex++;
	}
	if (curr){
		return currIndex ;
	}
	return 0;
}
static List yey;

void newui(){
	system("cls");
	cout<<"FIBONACCI SEQUENCE GENERATOR\n";
	cout<<"FIRST 90 FIBONACCI SEQUENCE\n";
	for(int p =0; p<=90; p++){
		cout<<"ELEMENT "<<p<<": ";
		cout<<fibos[p]<<"\n";
	}
	cout<<"===============================================\n";
}

void fiboLinkLister(unsigned long long int arr[10000], int x){
	system("cls");
	cout<<"ANG MGA FIBONACCI NUMBER MULA SA UNANG COLLATZ AY: \n";
	int y = --x;
	int listIndex = 0;
	for(int i = 2; i<=90; i++){
		for(int l = y; l>=0; l--){
			if(arr[l]==fibos[i]){
				cout<<arr[l]<<", ";
				fiboList[listIndex]=arr[l];
				listIndex++;
				break;
			}
		}
	}
	cout<<endl;
}

int sizer(unsigned long long int arr[10000]){
	int flag = 0;
	for (int i = 0; i<=10000; i++){
		if(arr[i]==1){
			flag++;
			return flag;
			break;
		}
		else flag++;
	}
}

int sizer1(unsigned long long int arr[10000]){
	int flag = 0;
	for (int i = 0; i<=1000; i++){
		if(arr[i]==1){
			flag++;
			return flag;
			break;
		}
		else if(arr[i]==0) continue;
		else flag++;
	}
}


int fibo(int size, unsigned long long int arr[100]){
	unsigned long long int t1 = 1, t2 = 1, nextTerm = 0;
    
	for (int i = 1; i <= size; ++i)
    {
        if(i == 1)
        {
            arr[0]=0;
            continue;
        }
        if(i == 2)
        {
        	arr[1]= 1;
        	arr[i]= 1;
            continue;
        }
        nextTerm = t1 + t2;
        t1 = t2;
        t2 = nextTerm;
        arr[i]=t2;
    }
	return arr[10000];	
}

int pali(int but){
	 int num, digit, rev = 0;
	 num = but;
	 do
     {
        digit = num % 10;
        rev = (rev * 10) + digit;
        num = num / 10;
     }while (num != 0);
     if(but==rev) return but;
     else return 0;
}


int collatzArray(unsigned long long int x, unsigned long long int arr[1000]){
	int idxr = 1;
	arr[0] = x;
	
	while(x != 1){
		if(x%2==1){
			x = (3 * x) + 1;
			arr[idxr] = x;
		}
		else{
			x /= 2;
			arr[idxr] = x;
		}
		idxr++;
	}
	return arr[1000];
}
void intro()
{	
	int fiboSize;
	unsigned long long int fib;
	cout<<"a.Add the sequence\nb.Delete a number\nc.Edit\nd.View\ne.Find a number\nf.Exit\n ";
	char choice;
	cin>>choice;
	cin.ignore(1,'\n');
	switch(choice){
		case 'a':
		case 'A':{
			int data;
			cout<<"Enter number to add: ";
			cin>>data;
			cin.ignore(1,'\n');
			yey.AddNode(data);
			cout<<"\nData added\n";		
			intro();
			break;
		}
		case 'b':
		case 'B':{
			yey.PrintList();
			cout<<"Enter data to be deleted: ";
			int delData;
			cin>>delData;
			cin.ignore(1,'\n');
			yey.DeleteNode(delData);
			cout<<"\nData deleted\n";
			intro();
			break;
		}
		case 'c':
		case 'C':{
			cout<<"Enter data to be edited: ";
			int edit;
			cin>>edit;
			cout<<"\nEnter new data: ";
			int newData;
			cin>>newData;
			yey.EditNode(edit,newData);
			cout<<"\nData edited\n";
			intro();
			break;
		}
		case 'd':
		case 'D':{
			cout<<"Contents of the list:\n";
			yey.PrintList();
			intro();
			break;
		}
		case 'e':
		case 'E':{
			cout<<"Enter data to be searched: ";
			int find;
			cin>>find;
			int index = yey.FindData(find);
			cout<<"The data "<<find<<" is in the "<<index<<" position"<<endl;
			intro();
			break;
		}
	}
}

int main(/*int argc, char** argv*/) {
	int fiboSize,arrSize1, arrSize2, sumOfArrLength,counter = 0, counter1 = 0;
	unsigned long long int input, fib, collArr1[1000], collArr2[1000], paliArr[1000], paliArr2[1000], p, x, y, o;
	
	cout<<"==================THIS IS A COLLATZ GENERATOR=================\n";
	cout<<"PLEASE INPUT A NUMBER(1-1000):  ";
	cin>>input;
	cin.ignore(1, '\n');
	cout<<"\n";
	if(input<=0){
		cout<<"Hindi pwede ang gusto mo!\n";
		return 0;
		system("pause");
	}
	collatzArray(input, collArr1);
	
	for(int i=0; i<=(sizeof(collArr1)/sizeof(*collArr1)); i++){
		x = collArr1[i];
		p = pali(x);
		if(collArr1[i]==1){
			cout<<"ELEMENT "<<i<<": "<<collArr1[i]<<"\n\n";
			paliArr[i] = 1;
			break;		
		}
		cout<<"ELEMENT "<<i<<": "<<x<<"\n";
		if(p==x){
			paliArr[i] = x;
		}
		else {
		paliArr[i]=0;
		continue;
		}
	}
	
	arrSize1 = sizer(collArr1);	
	collatzArray(arrSize1,collArr2);
	
	for(int j=0; j<=(sizeof(collArr2)/sizeof(*collArr2)); j++){	
		y = collArr2[j];
		o = pali(y);
		if(collArr2[j]==1){
			cout<<"ELEMENT "<<j<<": "<<collArr2[j]<<"\n\n";
			paliArr2[j] = 1;
			break;		
		}
		cout<<"ELEMENT "<<j<<": "<<y<<"\n";
		if(o==y){
			paliArr2[j] = y;
		}
		else {
		paliArr2[j]=0;
		continue;
		}
	}
	
	arrSize2 = sizer(collArr2);
	
	sumOfArrLength = arrSize1 + arrSize2;
	cout<<"THE SIZE OF ARRAY 1 IS: "<<arrSize1<<"\n";
	cout<<"THE SIZE OF ARRAY 2 IS: "<<arrSize2<<"\n";
	cout<<"THEIR SUM IS: "<<sumOfArrLength<<"\n\n===============================================================\n";
	
	system("pause");
	system("cls");
	
	cout<<"===============PALINDROMES=================\n\n";
	cout<<"SHOWING PALINDROMES IN ARRAY 1: \n";
	
	for(int k=0; k<=(sizeof(paliArr)/sizeof(*paliArr)); k++){
		if(paliArr[k]==0)cout<<"";
		else if(paliArr[k]==1){ 
			cout<<1<<"\n\n";
			break;
		}
		else{
		 cout<<paliArr[k]<<", ";
		}
	}
	
	counter = sizer1(paliArr);
	
	cout<<"SHOWING PALINDROME FOR ARRAY 2: \n";
	
	for(int k=0; k<=(sizeof(paliArr)/sizeof(*paliArr)); k++){
		if(paliArr2[k]==0)cout<<"";
		else if(paliArr2[k]==1){
			cout<<1<<"\n\n";
			break;
		}
		else{
		 cout<<paliArr2[k]<<", ";
		}
	}
	
	counter1 = sizer1(paliArr2);	
	
	sumOfArrLength = counter + counter1;
	cout<<"THE NO. OF ELEMENTS IN PALINDROME ARRAY 1 IS: "<<counter<<"\n";
	cout<<"THE NO. OF ELEMENTS IN PALINDROME ARRAY 2 IS: "<<counter1<<"\n";
	cout<<"THEIR SUM IS: "<<sumOfArrLength<<"\n\n===========================================\n";
	
	system("pause");
	
	fibo(90, fibos);
	newui();
	
	system("pause");
	
	fiboLinkLister(collArr2, arrSize2);
	
	system("pause");
	system("cls");
	
	cout<<"ADDING OF FIBO-NUMS FROM CUSTOM FUNCTION GENERATED COLLATZ\n";

	for(int j = 0; j<=100; j++){
		if(fiboList[j]==0)break;
		fib = fiboList[j];
		yey.AddNode(fib); 
	}
	cout<<"ANG LAMAN NG LINKED LIST AY: "<<endl;
	yey.PrintList();
	system("pause");
	system("cls");
	
	intro();
}
